/* class settings */
class Settings{
  /* constructor */
  constructor(){
    this.setupDefaults();
  }


  /* setup defaults */
  setupDefaults(){
    this.ovr=false;                     // game over status
    this.camt=18;                       // amount of cards (divideable by two)
    this.cols=6;                        // how many columns (devideable by two)
    this.nln=String.fromCharCode(10);   // line break
    this.btnaud='';                     // button audio
    this.clkaud='';                     // color button audio
    this.erraud='';                     // error audio
  }
}

/* class tools */
class Tools{
  /* constructor */
  constructor(set){
    this.SET=set;
  }


  /* set class to element */
  setClassToElement(elem,elmcls){
    elem.className=elmcls;
  }

  /* add class to element */
  addClassToElement(elem,elmcls){
    var cls=elem.className;
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      this.setClassToElement(elem,newcls);
    }
  }

  /* add class to element by id */
  addClassToElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    if(cls.includes(elmcls)==false){
      var newcls=cls.concat(' '+elmcls);
      document.getElementById(elmid).className=newcls;
    }
  }

  /* remove class from element by id */
  removeClassFromElementById(elmid,elmcls){
    var cls=this.getClassStringForElementById(elmid);
    var newcls=cls.split(elmcls).join('');
    document.getElementById(elmid).className=newcls;
  }

  /* get the string with class names for element */
  getClassStringForElementById(elmid){
    return document.getElementById(elmid).className;
  }

  /* get attribute for element by id */
  getAttributeValueForElementById(elem,attr){
    return document.getElementById(elem).getAttribute(attr);
  }

  /* set attribute for element by id */
  setAttributeValueForElementById(elem,attr,val){
    document.getElementById(elem).setAttribute(attr,val);
  }

  /* get inner html for element by id */
  getInnerHtmlForElementById(elem){
    return document.getElementById(elem).innerHTML;
  }

  /* set inner html for element by id */
  setInnerHtmlForElementById(elem,str){
    document.getElementById(elem).innerHTML=str;
  }

  /* do click on element by id */
  clickOnElementById(elmid){
    document.getElementById(elmid).click();
  }

  /* hide cover layer */
  hideCover(){
    this.addClassToElementById('cover1','cover1hide');
    this.addClassToElementById('cover2','cover2hide');
  }

  /* center game */
  centerWrapper(){
    var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
    document.getElementById('wrapper').style.marginLeft=left+'px';
    var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
    document.getElementById('wrapper').style.marginTop=top+'px';
    var btnw=document.getElementById('new').offsetWidth;
    var left=Math.floor(((document.getElementById('wrapper').offsetWidth)/2)-(btnw/2));
    document.getElementById('new').style.marginLeft=left+'px';
    document.getElementById('count').style.marginLeft=left+'px';
  }

  /* get items for selector */
  getItemsForSelector(sel){
    return document.querySelectorAll(sel);
  }

  /* remove item from array by key */
  removeItemFromArrayByKey(key,arr){
    arr.splice(key,1);
    return arr;
  }

  /* add item to array */
  addItemToArray(item,arr){
    arr.push(item);
    return arr;
  }

  /* play sound */
  playSound(snd){
    snd.cloneNode(true).play();
  }
}

/* class sound */
class Sound{
  /* constructor */
  constructor(){
    this.setupAudio();
  }


  /* setup game audio files */
  setupAudio(){
    this.btnaud=new Audio('button.wav');
    this.btnaud.volume=0.15;
    this.clkaud=new Audio('click.wav');
    this.clkaud.volume=0.5;
    this.erraud=new Audio('error.wav');
    this.erraud.volume=0.5;
  }
}

/* class game */
class Game{
  /* constructor */
  constructor(){
    /* objects */
    this.SET=new Settings();
    this.TLS=new Tools(this.SET);
    this.SND=new Sound();
    /* event listener */
    addEventListener('click',this);
    addEventListener('keydown',this);
    addEventListener('resize',this);
  }


  /* lock player input */
  lockInput(){
    this.TLS.setAttributeValueForElementById('wrapper','attr-lck',1);
  }

  /* unlock player input */
  unlockInput(){
    this.TLS.setAttributeValueForElementById('wrapper','attr-lck',0);
  }

  /* is input locked */
  isLocked(){
    if(parseInt(this.TLS.getAttributeValueForElementById('wrapper','attr-lck'))==1){return true;}
    return false;
  }

  /* step counter one up */
  setCounterUp(){
    this.TLS.setInnerHtmlForElementById('count',parseInt(this.TLS.getInnerHtmlForElementById('count'))+1);
  }

  /* reset step counter */
  resetCounter(){
    this.TLS.setInnerHtmlForElementById('count',0);
  }



  /* generate html */
  generateGridHtml(){
    console.log('generate grid html');
    var html='';
    // generate html
    for(var x=1;x<=this.SET.camt;x++){
      html+='      <div class="card" id="card-'+x+'"><div class="letter">-</div><div class="cbg"></div></div>'+this.SET.nln;
      if(x%this.SET.cols==0){html+='      <div class="clear"></div>'+this.SET.nln;}
    }
    // return
    return html;
  }

  /* generate letters on cards */
  generateLettersOnCards(){
    console.log('set letters on cards');
    // generate letters
    for(var l=0;l<(this.SET.camt/2);l++){
      var crds=0;
      while(crds<2){
        var rnd=Math.floor(Math.random()*this.SET.camt)+1;
        var items=this.TLS.getItemsForSelector('#card-'+rnd+'>.letter');
        items.forEach((item)=>{
          if(item.innerHTML=='-'){
            item.innerHTML=String.fromCharCode(65+l);
            crds++;
          }
        });
      }
    }
  }

  /* set html to container */
  setGridHtml(html){
    console.log('set grid html');
    this.TLS.setInnerHtmlForElementById('game',html);
  }

  /* set some rotation to cards */
  setAnglesOnCards(){
    console.log('set rotation on cards');
    /* set rotation angles */
    this.TLS.getItemsForSelector('.card').forEach((item)=>{
      var rnd=Math.floor(Math.random()*15)+10;
      var sgn=(Math.round(Math.random())==0)?'-':'+';
      item.style.transform='rotate('+sgn+rnd+'deg)';
    });
  }



  /* check clicked cards for being equal */
  checkCards(){
    console.log('check clicked cards');
    // clicked cards
    var items=this.TLS.getItemsForSelector('.flipped:not(.fixed)');
    // equal    -> it is a hit
    // not qual -> it is a miss
    if(items[0].innerHTML==items[1].innerHTML){return true;}
    return false;
  }

  /* flip card for itemid */
  flipCard(itemid){
    var items=this.TLS.getItemsForSelector('#'+itemid+'>.letter');
    this.TLS.addClassToElement(items[0],'flipped');
  }

  /* fix clicked cards */
  fixCards(){
    var items=this.TLS.getItemsForSelector('.flipped:not(.fixed)');
    items.forEach((item)=>{this.TLS.addClassToElement(item,'fixed');});
  }

  /* reset clicked cards */
  resetCards(){
    var items=this.TLS.getItemsForSelector('.flipped:not(.fixed)');
    items.forEach((item)=>{this.TLS.setClassToElement(item,'letter');});
  }

  /* reset game */
  resetGame(){
    // less columns for portrait on mobile
    if(window.innerWidth<640){this.SET.cols=3;}
    // set grid
    this.setGridHtml(this.generateGridHtml());
    // set letters with a bit of random angle
    this.generateLettersOnCards();
    this.setAnglesOnCards();
    // reset points
    this.resetCounter();
    // and ready to go
    this.unlockInput();
    this.SET.ovr=false;
  }

  /* check if game has ended */
  checkForEndGame(){
    console.log('check end game');
    // if all cards are 'fixed'
    if(this.TLS.getItemsForSelector('.fixed').length==this.SET.camt){this.endGame();}
  }

  /* end of game */
  endGame(){
    console.log('game ended');
    this.TLS.playSound(this.SND.erraud);
    this.SET.ovr=true;
    setTimeout(()=>{this.lockInput();},1000);
  }



  /* init needed stuff */
  init(){
    console.log('init');
    // reset everything
    this.resetGame();
    // center content wrapper
    this.TLS.centerWrapper();
    // reveal game
    this.TLS.hideCover();
    // ready for input
    this.unlockInput();
    console.log('ready!');
  }



  /* handle listener events */
  handleEvent(event) {
    event.preventDefault();
    switch(event.type) {
      case 'click':
        this.leftClickEvent(event);
        break;
      case 'keydown':
        this.keyEvent(event);
        break;
      case 'resize':
        this.resizeEvent(event);
        break;
    }
  }

  /* left mouse click */
  leftClickEvent(event){
    // clicked on a button
    if(event.target.matches('.button')){
      this.TLS.playSound(this.SND.btnaud);
      var action=event.target.getAttribute('attr-action');
      console.log(action);
      // what to do?
      if(action=='new'){this.init();}
    }
    // clicked on a card
    if(this.isLocked()==false&&(this.SET.ovr==false&&event.target.parentElement.matches('.card'))){
      this.TLS.playSound(this.SND.clkaud);
      // which card
      var itemid=event.target.parentElement.getAttribute('id');
      // if not clicked or already set to 'fixed'
      // - flip card (if flipable)
      // - lock input (block click) if two flipped
      // - check clicked cards to be equal
      // - counter one up
      // - unlock input
      // - check if game ended
      if(this.TLS.getItemsForSelector('#'+itemid+'>.letter:not(.flipped):not(.fixed)').length>0){
        this.flipCard(itemid);
        if(this.TLS.getItemsForSelector('.flipped:not(.fixed)').length==2){
          this.lockInput();
          if(this.checkCards()==true){
            this.fixCards();
          }else{
            setTimeout(()=>{this.resetCards();},500);
          }
          setTimeout(()=>{this.setCounterUp();},250);
          setTimeout(()=>{this.unlockInput();},250);
        }
      }
      this.checkForEndGame();
    }
  }

  /* key press event */
  keyEvent(event){
    switch(event.code){
      // n(ew)
      case 'KeyN':
        this.TLS.clickOnElementById('new');
        break;
    }
  }

  /* window resize event */
  resizeEvent(event){
    // restart game
    this.init();
  }
}



/* wait until document is fully loaded and ready to start */
let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    GME=new Game();
    GME.init();
  }
},250);
